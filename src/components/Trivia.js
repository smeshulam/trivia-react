import React, {useState} from 'react';
import PropTypes from 'prop-types';

const Trivia = props => {
    const [state, setState] = useState({
        questions: [
            {
                question: 'what is the first answer',
                answers: [
                    {
                        answer: '1',
                        isTrue: true  
                    },
                    {
                        answer: '2',
                        isTrue: false  
                    },
                    {
                        answer: '3',
                        isTrue: false  
                    }
                ]    
            },
            {
                question: 'what is the second answer',
                answers: [
                    {
                        answer: '1',
                        isTrue: false  
                    },
                    {
                        answer: '2',
                        isTrue: true  
                    },
                    {
                        answer: '3',
                        isTrue: false  
                    }
                ]    
            },
            {
                question: 'what is the third answer',
                answers: [
                    {
                        answer: '1',
                        isTrue: false  
                    },
                    {
                        answer: '2',
                        isTrue: false  
                    },
                    {
                        answer: '3',
                        isTrue: true  
                    }
                ]    
            }             
        ], 
        questionsIndex: 0,
        score: 0,
        completed: false
    });
    
    const [answers, setAnswers] = useState([])

    //Save checked answer index
    const answerChecked = (id) => () => {
        let answersCopy = JSON.parse(JSON.stringify(answers))
        answersCopy[state.questionsIndex] = id;
        setAnswers(answersCopy)
    }
    
    //Hadle back button
    const back = () => {
        if(state.questionsIndex > 0) {
            setState({...state, questionsIndex : state.questionsIndex - 1})
        }
    }

    //Returns "checked" if answer as allredy checked
    const isChecked = (id) => {
        const checkedAnswer = answers[state.questionsIndex]; 
        return checkedAnswer === id ? 'checked' : ''; 
    }

    //Handles next button
    const submitAnswer = (e) => {
        e.preventDefault();
        if(state.questionsIndex < state.questions.length -1) {
            setState({
                ...state,
                questionsIndex: parseInt(state.questionsIndex) + 1
            })
        } else {
            setState({...state, completed: true});
        }
    }

    //Returns how many answeers is correct
    const countAnswerdCorrectly = () => {
        const correctAnswersArr = [];
        //get all answers from question arr
        const answersArr = state.questions.map((question) => {
          return question.answers;
        })
        // map ou corrent answers
        answersArr.map((answers) => {
             answers.map((answer,i) => {
                if(answer.isTrue) {
                    correctAnswersArr.push(i+1);
                }
            })
        })
        //count corrent answers
        const count = correctAnswersArr.filter((correctIndex,i) => correctIndex === answers[i] )
        return count.length;
    }

    //Render Question
    const renderQuestion = () => {
        const {questions, questionsIndex} = state;
        const curQuestion = questions[questionsIndex];
        const curAnswers = curQuestion.answers;
        return(
        <React.Fragment>
                    <h2>{curQuestion.question}</h2>
                    <form className="answers-form" onSubmit={submitAnswer}>
                    <div>
                        {
                            curAnswers.map((answer, i) => (
                                <div key={i}>
                                <input
                                      type="radio"
                                      onChange={answerChecked(i + 1)}
                                      checked={isChecked(i + 1)}
                                      name="answer"
                                      value={i + 1}
                                    />
                                    {answer.answer}
                                </div>
                            ))
                        }
                    </div>
                    <input type="button" disabled={questionsIndex === 0} value="Back" onClick={back}/>
                    <input type="submit" value="Next"/>
                    </form>
                </React.Fragment>
        )
    }

   
    const {questions} = state;
    const completed = state.completed;
    if(!completed) {
        return renderQuestion();
    } else {
        return (
            <React.Fragment>
                <h1>completed!</h1>
                answered correctly: {countAnswerdCorrectly()} / {questions.length}
            </React.Fragment>
        )
    }
    
}

export default Trivia
import React from 'react';

export default function Header() {
    return (
      <React.Fragment>
        <header>
            <h1>My React Trivia</h1>
        </header>
      </React.Fragment>
    );
  }
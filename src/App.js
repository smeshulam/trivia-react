import React from 'react';

import Header from './layout/Header'
import Trivia from './components/Trivia'

import './App.css';

function App() {
  return (
    <React.Fragment>
      <Header/>
      <Trivia/>
    </React.Fragment>
  );
}

export default App;
